Bootstrap: docker
From: alpine:3.8

%labels
MAINTAINER Laurent Modolo

%post
MUSIC_VERSION=6613c53
SAMTOOLS_VERSION=1.7
PACKAGES="git \
             make \
             gcc \
             g++ \
             musl-dev \
             zlib-dev \
             ncurses-dev \
             bzip2-dev \
             xz-dev \
             ca-certificates \
             bash"

apk update && \
apk add ${PACKAGES}

curl -L -o samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    http://jaist.dl.sourceforge.net/project/samtools/samtools/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 \
 && tar jxvf samtools-${SAMTOOLS_VERSION}.tar.bz2  \
 && cd samtools-${SAMTOOLS_VERSION}/ \
 && ./configure --without-curses \
 && make \
 && make install

git clone https://github.com/gersteinlab/MUSIC.git && \
cd MUSIC && \
git checkout ${MUSIC_VERSION} && \
make clean && \
make  && \
cd .. && \
mv MUSIC/bin/MUSIC /usr/bin/ && \
mv MUSIC/bin/generate_multimappability_signal.csh /usr/bin/ && \
mv MUSIC/bin/run_MUSIC.csh /usr/bin/ && \
rm -Rf MUSIC

chmod +x /usr/bin/*

%environment
export MUSIC_VERSION=6613c53

%runscript
exec /bin/bash "$@"
