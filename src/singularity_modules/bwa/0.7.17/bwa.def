Bootstrap: docker
From: ubuntu:18.04

%labels
MAINTAINER Laurent Modolo

%post
BWA_VERSION=0.7.17
SAMBLASTER_VERSION=0.1.24

PACKAGES=curl="7.58.0* \
ca-certificates=20180409 \
build-essential=12.4* \
zlib1g-dev=1:1.2.11*"

apt-get update && \
apt-get install -y --no-install-recommends ${PACKAGES} && \
apt-get clean

curl -k -L https://github.com/biod/sambamba/releases/download/v${SAMBAMBA_VERSION}/sambamba_v${SAMBAMBA_VERSION}_linux.tar.bz2 -o sambamba_v${SAMBAMBA_VERSION}_linux.tar.bz2 && \
tar xvjf sambamba_v${SAMBAMBA_VERSION}_linux.tar.bz2 && \
mv sambamba /usr/bin/ && \
rm -R sambamba_v${SAMBAMBA_VERSION}_linux*

curl -k -L https://github.com/lh3/bwa/releases/download/v${BWA_VERSION}/bwa-${BWA_VERSION}.tar.bz2 -o bwa-v${BWA_VERSION}.tar.bz2 && \
tar xjf bwa-v${BWA_VERSION}.tar.bz2 && \
cd bwa-${BWA_VERSION}/ && \
make && \
cp bwa /usr/bin && \
cd .. && \
rm -R bwa-${BWA_VERSION}/

curl -k -L https://github.com/GregoryFaust/samblaster/releases/download/v.${SAMBLASTER_VERSION}/samblaster-v.${SAMBLASTER_VERSION}.tar.gz -o samblaster-v.${SAMBLASTER_VERSION}.tar.gz && \
tar xvf samblaster-v.${SAMBLASTER_VERSION}.tar.gz && \
cd samblaster-v.${SAMBLASTER_VERSION}/ && \
make && \
cp samblaster /usr/bin && \
cd .. && \
rm -R samblaster-v.${SAMBLASTER_VERSION}/

%environment
export BWA_VERSION=0.7.17
export SAMBLASTER_VERSION=0.1.24

%runscript
exec /bin/bash "$@"
