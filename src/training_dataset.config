profiles {
  docker {
    docker.temp = 'auto'
    docker.enabled = true
    process {
      withName: build_synthetic_bed {
        container = "bedtools:2.25.0"
        cpus = 1
      }
      withName: fasta_from_bed {
        container = "bedtools:2.25.0"
        cpus = 1
      }
      withName: index_fasta {
        container = "bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: mapping_fastq_paired {
        container = "bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: bam_2_fastq_paired {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_paired {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_paired {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: index_bam_paired {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: mapping_fastq_single {
        container = "bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: bam_2_fastq_single {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_single {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_single {
        container = "samtools:1.7"
        cpus = 4
      }
      withName: index_bam_single {
        container = "samtools:1.7"
        cpus = 4
      }
    }
  }
  singularity {
    singularity.enabled = true
    process {
      withName: build_synthetic_bed {
        container = "file://bin/bedtools:2.25.0.sif"
        cpus = 1
      }
      withName: fasta_from_bed {
        container = "file://bin/bedtools:2.25.0.sif"
        cpus = 1
      }
      withName: index_fasta {
        container = "file://bin/bowtie2:2.3.4.1.sif"
        cpus = 4
      }
      withName: mapping_fastq_single {
        container = "file://bin/bowtie2:2.3.4.1.sif"
        cpus = 4
      }
      withName: mapping_fastq_paired {
        container = "file://bin/bowtie2:2.3.4.1.sif"
        cpus = 4
      }
      withName: bam_2_fastq_paired {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: filter_bam_paired {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: sort_bam_paired {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: index_bam_paired {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: bam_2_fastq_single {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: filter_bam_single {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: sort_bam_single {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
      withName: index_bam_single {
        container = "file://bin/samtools:1.7.sif"
        cpus = 4
      }
    }
  }
  psmn {
    process{
      withName: build_synthetic_bed {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "bedtools/2.25.0"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = 'monointeldeb128'
      }
      withName: fasta_from_bed {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "bedtools/2.25.0"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = 'monointeldeb128'
      }
      withName: index_fasta {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "bowtie2/2.3.4.1"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "20GB"
        time = "12h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: mapping_fastq_paired {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "bowtie2/2.3.4.1:samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: bam_2_fastq_paired {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: sort_bam_paired {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: index_bam_paired {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: mapping_fastq_single {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "bowtie2/2.3.4.1:samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: bam_2_fastq_single {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: sort_bam_single {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
      withName: index_bam_single {
        beforeScript = "source /usr/share/lmod/lmod/init/bash; module use ~/privatemodules"
        module = "samtools/1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 16
        memory = "30GB"
        time = "24h"
        queue = 'E5-2670deb128A,E5-2670deb128B,E5-2670deb128C,E5-2670deb128D,E5-2670deb128E,E5-2670deb128F'
        penv = 'openmp16'
      }
    }
  }
}
